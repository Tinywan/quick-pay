define(function(require, exports, module) {
	var Vue = require('./js/vue.js');
	var common = require('./js/common.js');
	
	new Vue({
		el: '#main-container',
		data: {
		  	isWeixin: false,
		  	isAlipay: false
		},
		mounted: function() {
			var payType = common.getUrlParams().payType;
			if(!payType) payType = 'weixin';
			this.isWeixin = payType == 'weixin';
			this.isAlipay = payType == 'alipay';
		}
	});
});